﻿
# Pearlman Dynasty

pearlman_0001 = { # Henrik I Divenscourge, King of Pearlsedge
	name = "Henrik"
	dynasty = dynasty_divenscourge
	religion = skaldhyrric_faith
	culture = dalric
	
	trait = race_human
	trait = education_martial_4
	trait = ambitious
	trait = impatient
	trait = fickle
	trait = reaver
	trait = viking
	
	846.7.15 = {
		birth = yes
	}
	
	865.3.7 = {
		add_spouse = pearlman_0001_1
	}
	
	871.8.12 = {
		add_spouse = pearlman_0001_2
	}
	
	879.1.1 = {
		give_nickname = nick_divenscourge
	}
	
	885.11.30 = {
		death = "885.11.30"
	}
}

pearlman_0001_1 = {
	name = "Gyda"
	# lowborn
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = vengeful
	trait = lustful
	trait = shieldmaiden
	
	848.10.22 = {
		birth = yes
	}
	
	865.3.7 = {
		add_spouse = pearlman_0001
	}
	
	870.3.1 = {
		death = "870.3.1"
	}
}

pearlman_0001_2 = {
	name = "Astrid"
	# lowborn
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = calm
	trait = content
	trait = compassionate
	
	851.4.10 = {
		birth = yes
	}
	
	871.8.12 = {
		add_spouse = pearlman_0001
	}
	
	893.9.29 = {
		death = "893.9.29"
	}
}

pearlman_0002 = { # Haakon I Henriksson, King of Pearlsedge
	name = "Haakon"
	dynasty = dynasty_divenscourge
	religion = skaldhyrric_faith
	culture = dalric
	
	trait = race_human
	trait = education_martial_2
	trait = temperate
	trait = stubborn
	trait = forgiving
	trait = reaver
	trait = viking
	
	father = pearlman_0001
	mother = pearlman_0001_1
	
	863.5.7 = {
		birth = yes
	}
	
	901.2.19 = {
		death = "901.2.19"
	}
}

pearlman_0003 = { # Adela Haakonsdatter, daughter of Haakon I
	name = "Adela"
	dynasty = dynasty_divenscourge
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = gregarious
	trait = lazy
	trait = craven
	
	father = pearlman_0002
	
	885.4.26 = {
		birth = yes
	}
	
	937.6.8 = {
		death = "937.6.8"
	}
}

pearlman_0004 = { # Halla Henriksdatter, daughter of Henrik Divenscourge, wife of Gunnar of Deranne
	name = "Halla"
	dynasty = dynasty_divenscourge
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = cynical
	trait = arbitrary
	trait = wrathful
	
	father = pearlman_0001
	mother = pearlman_0001_1
	
	865.3.3 = {
		birth = yes
	}
	
	888.3.9 = {
		add_spouse = deranne_0003
	}
	
	925.9.22 = {
		death = "925.9.22"
	}
}

pearlman_0005 = { # Hallfred Henriksson, son of Henrik Divenscourge
	name = "Hallfred"
	dynasty = dynasty_divenscourge
	religion = skaldhyrric_faith
	culture = dalric
	
	trait = race_human
	trait = education_diplomacy_1
	trait = humble
	trait = content
	trait = gluttonous
	trait = clubfooted
	
	father = pearlman_0001
	mother = pearlman_0001_2
	
	873.6.2 = {
		birth = yes
	}
	
	904.3.7 = {
		death = {
			death_reason = death_disappearance # Went west, never returned
		}
	}
}

pearlman_0006 = { # Harald Pearlman, King of Pearlsedge
	name = "Harald"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_stewardship_3
	trait = diligent
	trait = compassionate
	trait = honest
	
	father = pearlman_0001
	mother = pearlman_0001_2
	
	879.6.28 = {
		birth = yes
	}
	
	934.7.12 = {
		death = "934.7.12"
	}
}

tretunis_1000 = { # Andarta Tretunis, princess of Tretun, wife of Harald Pearlman
	name = "Andarta"
	dynasty = dynasty_tretunis
	religion = west_damish
	culture = tretunic
	female = yes

	father = tretunis_0017
	
	trait = race_human
	trait = education_diplomacy_2
	trait = gregarious
	trait = forgiving
	trait = cynical
	
	879.6.29 = {
		birth = yes
	}
	
	940.1.3 = {
		death = "940.1.3"
	}
}

pearlman_0007 = { # Ernmund I Pearlman, King of Pearlsedge
	name = "Ernmund"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_martial_2
	trait = temperate
	trait = craven
	trait = shy
	
	father = pearlman_0006
	mother = tretunis_1000
	
	900.10.19 = {
		birth = yes
	}
	
	916.10.20 = {
		add_spouse = rubentis_0024
	}
	
	947.05.07 = {
		death = "947.05.07"
	}
}

pearlman_0008 = { # Henrik II Pearlman, King of Pearlsedge
	name = "Henrik"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_martial_2
	trait = ambitious
	trait = zealous
	trait = stubborn
	
	father = pearlman_0007
	mother = rubentis_0024
	
	924.05.08 = {
		birth = yes
	}
	
	953.05.30 = {
		death = "953.05.30"
	}
}

pearlman_0009 = { # Ernmund II Pearlman, King of Pearlsedge
	name = "Ernmund"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_learning_2
	trait = chaste
	trait = gluttonous
	trait = forgiving
	
	father = pearlman_0007
	mother = rubentis_0024
	
	937.08.17 = {
		birth = yes
	}
	
	971.01.13 = {
		death = "971.01.13"
	}
}

pearlman_0010 = { # Haakon II Pearlman, King of Pearlsedge
	name = "Haakon"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_stewardship_1
	trait = brave
	trait = impatient
	trait = stubborn
	trait = arbitrary
	
	father = pearlman_0009
	
	958.12.17 = {
		birth = yes
	}
	
	994.03.13 = {
		death = "994.03.13"
	}
}

pearlman_0011 = { # Serek "Seaborn" Pearlman, Count of Stingport
	name = "Serek"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = arbitrary
	trait = callous
	trait = forder
	
	father = pearlman_0009
	
	969.08.23 = {
		birth = yes
		give_nickname = nick_seaborn
		effect = {
			add_character_flag = bald_head
		}
	}
}

pearlman_0012 = { # Tristan Pearlman, son of Serek
	name = "Tristan"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_diplomacy_2
	trait = honest
	trait = chaste
	trait = cynical
	
	father = pearlman_0011
	
	994.10.06 = {
		birth = yes
	}
}

pearlman_0013 = { # Aron Pearlman, King of Pearlsedge
	name = "Aron"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	diplomacy = 4
	martial = 12
	stewardship = 7
	intrigue = 2
	learning = 5
	
	trait = race_human
	trait = education_martial_4
	trait = patient
	trait = brave
	trait = lustful
	trait = giant
	trait = organizer
	
	father = pearlman_0010
	
	980.4.13 = {
		birth = yes
		effect = {
			if = {
				limit = { has_game_rule = enable_marked_by_destiny }
				add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
			}
		}
	}
	
	994.03.13 = {
		effect = {
			set_artifact_rarity_illustrious = yes
			create_artifact = {
				name = pearl_of_the_dame_name
				description = pearl_of_the_dame_description
				type = pedestal
				visuals = diamond
				template = pearl_of_the_dame_template
				wealth = scope:wealth
				quality = scope:quality
				history = {
					actor = character:pearlman_0010
					recipient = character:pearlman_0013
					type = inherited
					date = 994.03.13
				}
				modifier = pearl_of_the_dame_modifier
				save_scope_as = newly_created_artifact
			}
			scope:newly_created_artifact = {
				set_variable = { name = historical_unique_artifact value = yes }
			}
			add_character_flag = bald_head
		}
	}
}

pearlman_0014 = { # Alec Pearlman, son of Aron
	name = "Alec"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_stewardship_2
	trait = greedy
	trait = just
	trait = zealous
	
	father = pearlman_0013
	
	999.9.1 = {
		birth = yes
	}
	
	1017.3.5 = {
		add_spouse = 517 # Matilda of Lanpool
	}
}

pearlman_0015 = { # Haakon Pearlman, son of Alec
	name = "Haakon"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	
	father = pearlman_0014
	mother = 517
	
	1021.2.5 = {
		birth = yes
	}
}

pearlman_0016 = { # Astrid Pearlman, wife of Halfdal II of Tef
	name = "Astrid"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = gregarious
	trait = lustful
	trait = impatient
	
	father = pearlman_0006
	mother = tretunis_1000
	
	903.05.16 = {
		birth = yes
	}
	926.3.17 = {
		add_spouse = taefdares_0005
	}
	970.10.7 = {
		death = yes
	}
}

pearlman_0017 = { # Klara Pearlman
	name = "Klara"
	dynasty_house = house_pearlman
	religion = west_damish
	culture = pearlsedger
	female = yes
	
	trait = race_human
	trait = education_learning_1
	trait = shy
	trait = temperate
	trait = content
	
	father = pearlman_0010
	
	983.10.1 = {
		birth = yes
	}
	
	999.10.2 = {
		add_spouse = cliffman_0003
	}
}

## Pearlsguard dynasty, of The Pearls

pearlsguard_0001 = { # former Count Alfr
	name = "Alfr"
	dynasty_ = dynasty_pearlsguard
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_diplomacy_2
	trait = brave
	trait = calm
	trait = arrogant
	
	958.11.1 = {
		birth = yes
	}
	
	1005.6.30 = {
		death = "1005.6.30"
	}
}

pearlsguard_0002 = { # Count Edvard
	name = "Edvard"
	dynasty = dynasty_pearlsguard
	religion = west_damish
	culture = pearlsedger
	
	trait = race_human
	trait = education_martial_4
	trait = callous
	trait = calm
	trait = diligent
	
	father = pearlsguard_0001
	
	982.1.19 = {
		birth = yes
	}
	
}

pearlsguard_0003 = { # Varina Pearlsguard
	name = "Varina"
	dynasty = dynasty_pearlsguard
	religion = west_damish
	culture = pearlsedger
	female = yes
	
	trait = race_human
	trait = education_martial_3
	trait = wrathful
	trait = just
	trait = impatient
	
	father = pearlsguard_0001
	
	983.10.1 = {
		birth = yes
	}
	
}