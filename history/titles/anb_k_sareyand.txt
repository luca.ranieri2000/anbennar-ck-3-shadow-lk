k_sareyand = {
	1006.6.22 = {
		liege = e_bulwar
		holder = sun_elvish0006	# Eledas I Sarelzuir
	}
}

d_sareyand = {
	1006.6.22 = {
		liege = k_sareyand
		holder = sun_elvish0006	# Eledas I Sarelzuir
	}
}

c_sareyand = {
	1000.1.1 = { change_development_level = 14 }
	1006.6.22 = {
		liege = d_sareyand
		holder = sun_elvish0006	# Eledas I Sarelzuir
	}
}

c_zanbar = {
	1000.1.1 = { change_development_level = 17 }
}

c_kuzkumar = {
	1000.1.1 = { change_development_level = 12 }
}

c_harklum = {
	1000.1.1 = { change_development_level = 17 }
}

c_traz_suran = {
	1000.1.1 = { change_development_level = 12 }
}

c_surzan = {
	1000.1.1 = { change_development_level = 14 }
}

c_duklum = {
	1000.1.1 = { change_development_level = 14 }
}