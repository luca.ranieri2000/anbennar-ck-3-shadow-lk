k_hasr = {
	1014.4.12 = {
		holder = bulwari0010 #Udeš Ašrat
	}
}

c_eduz_vacyn = {
	1000.1.1 = { change_development_level = 19 }
}

c_kufu = {
	1000.1.1 = { change_development_level = 16 }
}

c_panubar = {
	1000.1.1 = { change_development_level = 15 }
}

c_durklum = {
	1000.1.1 = { change_development_level = 15 }
}

c_utlazna = {
	1000.1.1 = { change_development_level = 14 }
}

c_asranaz = {
	1000.1.1 = { change_development_level = 5 }
}

c_zankumar = {
	1000.1.1 = { change_development_level = 17 }
}

c_hasr = {
	1000.1.1 = { change_development_level = 20 }
	1014.4.12 = {
		holder = bulwari0010 #Udeš Ašrat
	}
}

c_dazar_suran = {
	1000.1.1 = { change_development_level = 17 }
}