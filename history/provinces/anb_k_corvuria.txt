#k_corvuria
##d_bal_dostan
###c_arca_corvur
441 = {		#Arca Corvur

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_dostan_01
		special_building = castanorian_citadel_bal_dostan_01
	}
}
2220 = {	#Olmabutari

    # Misc
    holding = none

    # History

}
2221 = {	#Village of Corvuria

    # Misc
    holding = city_holding

    # History
}
2222 = {	#Garsibuta

    # Misc
    holding = church_holding

    # History

}

###c_mihaelas_redoubt
440 = {		#Mihaela's Redoubt

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2223 = {	#Rudasite

    # Misc
    holding = church_holding

    # History

}
2224 = {	#Nichargira

    # Misc
    holding = none

    # History

}

###c_elderwright
439 = {		#Elderwright

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2225 = {	#Inomedece

    # Misc
    holding = city_holding

    # History

}
2226 = {	#Cepurest

    # Misc
    holding = none

    # History

}
2227 = {	#Girenceul

    # Misc
    holding = none

    # History

}

###c_corargin
438 = {		#Corargin

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2228 = {

    # Misc
    holding = none

    # History

}

##d_blackwoods
###c_blackwoods
435 = {		#Blackwoods

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2212 = {	#Burisgilusi

    # Misc
    holding = none

    # History

}
2213 = {	#Gercopacuri

    # Misc
    holding = none

    # History

}

###c_kortir
437 = {		#Kortir

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2217 = {	#Doinpuri

    # Misc
    holding = none

    # History

}
2218 = {	#Visebitari

    # Misc
    holding = none

    # History

}
2219 = {	#Barsanburdavan

    # Misc
    holding = city_holding

    # History

}

###c_karns_hold
436 = {		#Karn's Hold

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2214 = {	#Brucalcur

    # Misc
    holding = none

    # History

}
2215 = {	#Codeseimul

    # Misc
    holding = church_holding

    # History

}
2216 = {	#Rogeresampe

    # Misc
    holding = none

    # History

}

###c_holstead
2211 = {		#Holstead

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
429 = {		#Lonelyoak

    # Misc
    holding = city_holding

    # History

}
2210 = {	#Bruclauta

    # Misc
    holding = none

    # History

}

##d_tiferben
###c_gerbuta
426 = {		#Gerbuta

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2187 = {	#Darascaru

    # Misc
    holding = church_holding

    # History

}
2188 = {	#Asalucra

    # Misc
    holding = none

    # History

}
2189 = {	#Bronhrispi

    # Misc
    holding = city_holding

    # History

}

###c_rotwall
423 = {		#Rotwall

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2182 = {	#Gelapura

    # Misc
    holding = none

    # History

}

###c_ebrosfeld
425 = {		#Ebrosfeld

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2183 = {	#Nerat's Pass

    # Misc
    holding = church_holding

    # History

}
2184 = {	#Albostir

    # Misc
    holding = none

    # History

}

###c_ioans_ford
427 = {		#Ioan's Ford

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2185 = {	#Mendrutadavan

    # Misc
    holding = city_holding

    # History

}
2186 = {	#Grocestrun

    # Misc
    holding = none

    # History

}

###c_cinuder
430 = {		#Cinuder

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2192 = {	#Niuteri

    # Misc
    holding = church_holding

    # History

}
2193 = {	#Escerumel

    # Misc
    holding = none

    # History

}
2195 = {	#Kalasermas

    # Misc
    holding = city_holding

    # History

}

###c_rackmans_court
428 = {		#Rackman's Court

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2190 = {	#Gascbór

    # Misc
    holding = church_holding

    # History

}
2191 = {	#Gujstaig

    # Misc
    holding = none

    # History

}

##d_ravenhill
###c_ravenhill
431 = {		#Ravenhill

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2196 = {	#Dascudizal

    # Misc
    holding = city_holding

    # History

}
2197 = {	#Ordmire

    # Misc
    holding = none

    # History

}

###c_rasidiad
432 = {		#Rasidiad

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2198 = {	#Peresmasla

    # Misc
    holding = city_holding

    # History

}
2199 = {	#Albovern

    # Misc
    holding = church_holding

    # History

}
2201 = {	#Dorlazas

    # Misc
    holding = none

    # History

}
2202 = {	#Șuditgira

    # Misc
    holding = none

    # History

}

###c_gablaine
433 = {		#Gablaine

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2203 = {	#Gujzibuta

    # Misc
    holding = church_holding

    # History

}
2204 = {	#Clothcetun

    # Misc
    holding = city_holding

    # History

}
2205 = {	#Tchàvar

    # Misc
    holding = castle_holding

    # History

}
2206 = {	#Zenacopac

    # Misc
    holding = none

    # History

}

###c_arca_kaldere
434 = {		#Arca Kaldere

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2207 = {	#Vulsinvar

    # Misc
    holding = church_holding

    # History

}
2208 = {	#Cintenosto

    # Misc
    holding = none

    # History

}
2209 = {	#Ciobiseán

    # Misc
    holding = none

    # History

}
