#k_elizna
##d_ibtatu
###c_ibtat
496 = {     #Ibtat

    # Misc
    culture = east_divenori
    religion = khetarchy
	holding = castle_holding

    # History
}

494 = {     #Rosutaaru

    # Misc
    culture = east_divenori
    religion = khetarchy
	holding = castle_holding

    # History
}

##d_arag_drolas
###c_drolaspand
580 = {		#Quznaram

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = city_holding

    # History
}

6492 = {		#Sassasah

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = none

    # History
}

6493 = {		#Drolaspand

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = castle_holding

    # History
}

6494 = {		#Gabar

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = none

    # History
}

###c_betelis
577 = {		#Betelis

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = castle_holding

    # History
}

6490 = {		#Eduz-Dimer

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = church_holding

    # History
}

6491 = {		#Puzurkar

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = none

    # History
}

###c_drolaklum
574 = {		#Drolaklum

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = castle_holding

    # History
}

6488 = {		#Rakhmatasi

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = none

    # History
}

6489 = {		#Turamakar

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = city_holding

    # History
}

###c_girakub
582 = {		#Azka Girakeru

    # Misc
    culture = east_divenori
    religion = cult_of_the_eseral_mitellu
	holding = castle_holding

    # History
}

6495 = {		#Musah

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = none

    # History
}

6496 = {		#Tamarhar

    # Misc
    culture = drolasesi
    religion = way_of_the_sea
	holding = city_holding

    # History
}

##d_baru_drolas
###c_dromaz
573 = {		#Dromaz

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6483 = {		#Karkare

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6484 = {		#Turaklam

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

6485 = {		#Pirqurkost

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

###c_kurmod
578 = {		#Kurmod

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6486 = {		#Ispaneklu

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = church_holding

    # History
}

6487 = {		#Malkentir

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

##d_annail
###c_annail
6673 = {		#Siyithum

    # Misc
    culture = ayabnati
    religion = way_of_the_sea
	holding = castle_holding

    # History
}

6674 = {		#Iwansah

    # Misc
    culture = ayabnati
    religion = way_of_the_sea
	holding = none

    # History
}

6675 = {		#Asrayar

    # Misc
    culture = ayabnati
    religion = way_of_the_sea
	holding = none

    # History
}

6676 = {		#Kyblilthar

    # Misc
    culture = ayabnati
    religion = way_of_the_sea
	holding = city_holding

    # History
}

6677 = {		#Bar-Kisad

    # Misc
    culture = ayabnati
    religion = way_of_the_sea
	holding = city_holding

    # History
}

6678 = {		#Puhiya

    # Misc
    culture = ayabnati
    religion = way_of_the_sea
	holding = church_holding

    # History
}

###c_pir_ail
533 = {		#Pir-Ail

    # Misc
    culture = east_divenori
    religion = cult_of_the_eseral_mitellu
	holding = castle_holding

    # History
}