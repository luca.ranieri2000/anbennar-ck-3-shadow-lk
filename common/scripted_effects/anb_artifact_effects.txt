﻿#Creates a new elven compass artifact
discover_elven_compass_effect = {
	set_artifact_rarity_masterwork = yes
	create_artifact = {
		name = elven_compass_name
		description = compass_of_$SHIP$_description
		type = miscellaneous
		visuals = pocket_fancy_case
		wealth = scope:wealth
		quality = scope:quality
		history = {
			actor = $ACTOR$
			recipient = $RECIPIENT$
			type = created
			date = $DATE$
			location = $LOCATION$
		}
		modifier = elven_compass_modifier
	}
}

inherit_elven_compass_effect = {
	set_artifact_rarity_masterwork = yes
	create_artifact = {
		name = elven_compass_name
		description = compass_of_$SHIP$_description
		type = miscellaneous
		visuals = pocket_fancy_case
		wealth = scope:wealth
		quality = scope:quality
		history = {
			recipient = $RECIPIENT$
			type = inherited
			date = $DATE$
		}
		modifier = elven_compass_modifier
	}
}