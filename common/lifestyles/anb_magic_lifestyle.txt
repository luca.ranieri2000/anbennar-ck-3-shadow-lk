﻿magic_lifestyle = {
	is_valid = {
		has_trait = magical_affinity
	}

	xp_per_level = 1000
	base_xp_gain = 25
}