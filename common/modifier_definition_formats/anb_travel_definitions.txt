﻿dwarven_hold_travel_danger = {
	prefix = MOD_TRAVEL_DANGER_PREFIX
	decimals = 0
	color = bad
}

dwarven_hold_surface_travel_danger = {
	prefix = MOD_TRAVEL_DANGER_PREFIX
	decimals = 0
	color = bad
}

dwarven_road_travel_danger = {
	prefix = MOD_TRAVEL_DANGER_PREFIX
	decimals = 0
	color = bad
}

cavern_travel_danger = {
	prefix = MOD_TRAVEL_DANGER_PREFIX
	decimals = 0
	color = bad
}